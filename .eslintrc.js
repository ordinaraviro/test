module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: ['@typescript-eslint'],
  extends: ['airbnb-base', 'plugin:@typescript-eslint/recommended'],
  rules: {
    '@typescript-eslint/explicit-gunction-return-type': 'off',
    'linebreak-style': 'off',
  },
};
