import express, { Request, Response } from 'express';
import dotenv from 'dotenv';

dotenv.config();

const app = express();
const port = process.env.PORT || 3000;

app.get('/', (req: Request, res: Response) => {
  res.send('Hello, this is my Express and TypeScript server!');
});

app.get('/multiplyByTwo', (req: Request, res: Response) => {
  const { number } = req.query;

  if (number && !Number.isNaN(Number(number))) {
    const result = Number(number) * 2;
    res.send(`The result of multiplying ${number} by 2 is: ${result}`);
  } else {
    res.status(400).send('Invalid or missing "number" query parameter.');
  }
});

app.listen(port, () => {
  console.log(`Server running on port ${port}`);
});
